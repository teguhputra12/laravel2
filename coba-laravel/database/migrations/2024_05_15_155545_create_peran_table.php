<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('peran', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cinemas_id');
            $table->unsignedBigInteger('casts_id');
            $table->string('nama',45);
            

            $table->foreign('cinemas_id')->references('id')->on('cinema')->onDelete('cascade');
            $table->foreign('casts_id')->references('id')->on('cast')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('peran');
    }
};
