<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function showRegistrationForm()
    {
        return view('register');
    }

    public function register(Request $request)
    {
        // Proses validasi data dan penyimpanan pengguna
        return redirect('/welcome')->with([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name')
        ]);
    }

    public function welcome(Request $request)
    {
        $data = $request->session()->all();
        return view('welcome', $data);
    }
}
