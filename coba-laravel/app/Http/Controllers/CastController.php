<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cast;

class CastController extends Controller
{
    public function index()
    {
        $casts = Cast::all();
        return view('casts.index', compact('casts'));
    }

    public function create()
    {
        return view('casts.create');
    }

    public function store(Request $request)
    {
        // Validasi data permintaan yang masuk
        $validatedData = $request->validate([
            'nama' => 'required',
            'umur' => 'required|numeric',
            'bio' => 'required',
        ]);

        // Buat instance baru dari Cast
        $cast = new Cast();
        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;
        $cast->save();

        // Redirect ke halaman yang relevan setelah menyimpan data
        return redirect()->route('cast.index')->with('success', 'Data pemain berhasil disimpan.');
    }

    public function show($cast_id)
    {
        $cast = Cast::findOrFail($cast_id); // Mengambil data pemain  berdasarkan id
        
        return view('casts.show', ['cast' => $cast]); // Menampilkan tampilan detail dengan data pemain film
    }

        public function edit($cast_id)
    {
        // Ambil data pemain  dari database berdasarkan $cast_id
        $cast = Cast::findOrFail($cast_id);
        
        // Tampilkan view untuk mengedit data dengan menyertakan data pemain  yang ingin diedit
        return view('casts.edit', compact('cast'));
    }


        public function update(Request $request, $cast_id)
    {
        // Temukan data pemain  berdasarkan $cast_id
        $cast = Cast::findOrFail($cast_id);

        // Update data pemain 
        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;
        $cast->save();

        // Redirect ke halaman yang relevan setelah memperbarui data
        return redirect()->route('cast.index')->with('success', 'Data pemain berhasil diperbarui.');
    }

        public function destroy($cast_id)
    {
        // Temukan data pemain  berdasarkan $cast_id
        $cast = Cast::findOrFail($cast_id);

        // Hapus data pemain 
        $cast->delete();

        // Redirect ke halaman yang relevan setelah menghapus data
        return redirect()->route('cast.index')->with('success', 'Data pemain berhasil dihapus.');
    }
}
