@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h1 class="mb-0">Detail Pemain</h1>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h2>{{ $cast->nama }}</h2>
                            <p><strong>Umur:</strong> {{ $cast->umur }}</p>
                            <p><strong>Biografi:</strong> {{ $cast->bio }}</p>
                        </div>
                    </div>
                </div>
                
                <!-- Bagian footer untuk tombol edit dan hapus -->
                <div class="card-footer d-flex justify-content-center">
                    <a href="{{ route('cast.edit', ['cast_id' => $cast->id]) }}" class="btn btn-primary me-2">
                        <i class="bi bi-pencil-square"></i> Edit
                    </a>
                    
                    <form action="{{ route('cast.destroy', ['cast_id' => $cast->id]) }}" method="POST" class="d-inline">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus pemain film ini?')">
                            <i class="bi bi-trash"></i> Hapus
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
