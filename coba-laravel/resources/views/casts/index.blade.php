@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    List Data Pemain 
                    <a href="{{ route('cast.create') }}" class="btn btn-primary float-end">Tambah Pemain Baru</a>
                </div>

                <div class="card-body">
                    <div class="table-responsive"> <!-- Tambahkan kelas untuk membuat tabel responsif -->
                        <table class="table table-striped table-hover"> <!-- Tambahkan kelas untuk membuat garis-garis tabel dan efek hover -->
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nama</th>
                                    <th>Umur</th>
                                    <th>Bio</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($casts as $cast)
                                <tr>
                                    <td>{{ $cast->id }}</td>
                                    <td><a href="{{ route('cast.show', ['cast_id' => $cast->id]) }}">{{ $cast->nama }}</a></td> <!-- Tautkan nama ke halaman detail -->
                                    <td>{{ $cast->umur }}</td>
                                    <td>{{ $cast->bio }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection